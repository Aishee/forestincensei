package main

import (
	"bytes"
	"html/template"
	"log"
	"time"

	"bitbucket.org/Aishee/forestincensei/src/story"
)

const rssTemplate = `
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
	<atom:link href="#" rel="self" type="application/rss+xml" />
	<title>Stories Incognito</title>
	<link>https://research.soulsec.net</link>
	<description></description>
	<generator></generator>
	<language>eng</language>
	<managingEditor>ed@soulsec.net (Stories Incognito Editors)</managingEditor>
	<webMaster>w@soulsec.net</webMaster>
	<copyright>Copyright 2019 Stories Incognito</copyright>
	<lastBuildDate>{{ .Date.Format "Mon, 02 Jan 2006 00:00:00 -0700" }}</lastBuildDate>
	{{ range .Stories }}<item>
		<title>{{ .Topic }}</title>
		<link>https://soulsec.net/read/story/{{ .ID }}</link>
		<pubDate>{{ .DatePublished.Format "Mon, 02 Jan 2006 00:00:00 -0700" }}</pubDate>
		<guid>https://soulsec.net/read/story/{{ .ID }}</guid>
		<description>{{ .Description }}</description>
	</item>{{ end }}
</channel>
</rss>
`

func RSS() string {
	funcMap := template.FuncMap{
		"slugify":   slugify,
		"unslugify": unslugify,
	}

	// Create a template, add the function map, and parse the text.
	tmpl, err := template.New("rss").Funcs(funcMap).Parse(rssTemplate)
	if err != nil {
		log.Fatalf("parsing: %s", err)
	}

	type RSSData struct {
		Stories []story.Story
		Date    time.Time
	}

	s, _ := story.ListPublished()
	rss := RSSData{
		Date:    time.Now(),
		Stories: s,
	}
	// Run the template to verify the output.
	var tpl bytes.Buffer
	err = tmpl.Execute(&tpl, rss)
	if err != nil {
		log.Fatalf("execution: %s", err)
	}
	return tpl.String()
}

const siteMapTemplate = `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
{{ range .Topics }}<url>
<loc>https://soulsec.net/read/topic/{{ .Name | slugify }}</loc>
<lastmod>{{ $.Date.Format "2006-01-02T00:00:00-07:00" }}</lastmod>
<changefreq>monthly</changefreq>
<priority>1</priority>
</url>{{ end }}
{{ range .Stories }}<url>
<loc>https://soulsec.net/read/story/{{ .ID }}</loc>
<lastmod>{{ .DatePublished.Format "2006-01-02T15:04:05-07:00" }}</lastmod>
<changefreq>monthly</changefreq>
<priority>0.75</priority>
</url>{{ end }}
</urlset>`

func SiteMap() string {
	funcMap := template.FuncMap{
		"slugify":   slugify,
		"unslugify": unslugify,
	}

	// Create a template, add the function map, and parse the text.
	tmpl, err := template.New("sitemap").Funcs(funcMap).Parse(siteMapTemplate)
	if err != nil {
		log.Fatalf("parsing: %s", err)
	}

	type Data struct {
		Stories []story.Story
		Topics  []thread.Topic
		Date    time.Time
	}

	s, _ := story.ListPublished()
	t, _ := topic.Load(TopicDB)
	data := Data{
		Date:    time.Now(),
		Stories: s,
		Topics:  t,
	}
	// Run the template to verify the output.
	var tpl bytes.Buffer
	err = tmpl.Execute(&tpl, data)
	if err != nil {
		log.Fatalf("execution: %s", err)
	}
	return tpl.String()
}
