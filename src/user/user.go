package user

import (
	"log"
	"time"

	"bitbucket.org/Aishee/forestincensei/src/utils"
	"github.com/asdine/storm"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var DB string

func init() {
	DB = "users.db"
	Add("anonymous", "english", false)
}

type User struct {
	ID         string `storm:"unique"`
	Email      string `storm:"unique"`
	Language   string
	Subscribed bool
	Joined     time.Time
}

func AnonymousUserID() string {
	userID, err := GetID("anonymous")
	if err != nil {
		log.Fatal(err)
	}
	return userID
}

func Add(email, language string, subscribed bool) (err error) {
	uuid := &User{
		ID:         utils.NewAPIKey(),
		Email:      email,
		Language:   language,
		Subscribed: subscribed,
		Joined:     time.Now(),
	}
	logrus.WithFields(logrus.Fields{
		"func": "user.Add",
	}).Infof("userid:%s email:%s", uuid.ID, uuid.Email)
	db, err := storm.Open(DB)
	defer db.Close()
	if err != nil {
		return
	}
	err = db.Save(uuid)
	if err == storm.ErrAlreadyExists {
		err = errors.Wrap(err, "'"+email+"' is taken!")
	}
	return
}

func Update(id, email, language string, subscribed bool) (err error) {
	db, err := storm.Open(DB)
	defer db.Close()
	if err != nil {
		return
	}
	err = db.Update(&User{ID: id, Email: email, Language: language})
	if err != nil {
		err = errors.Wrap(err, "Error updating! Please check issue!")
	}
	err = db.UpdateField(&User{ID: id}, "Subscribed", subscribed)
	if err != nil {
		err = errors.Wrap(err, "Error updating! Please check issue!")
	}
	return
}

func Get(id string) (uuid User, err error) {
	db, err := storm.Open(DB)
	defer db.Close()
	if err != nil {
		return
	}
	err = db.One("ID", id, &uuid)
	return
}

func GetID(email string) (userID string, err error) {
	db, err := storm.Open(DB)
	defer db.Close()
	if err != nil {
		return
	}
	var u User
	err = db.One("Email", email, &uuid)
	if err != nil {
		return
	}
	userID = uuid.ID
	return
}

func UserExists(id string) bool {
	_, err := Get(id)
	return err == nil
}

func All() (uuid []User, err error) {
	db, err := storm.Open(DB)
	defer db.Close()
	if err != nil {
		err = errors.Wrap(err, "Error open DB! Please check issue!")
		return
	}
	err = db.AllByIndex("ID", &uuid)
	if err != nil {
		err = errors.Wrap(err, "Error get all by ID! Please check issue!")
	}
	return
}
