package utils

import (
	"math/rand"
	"time"
)

const letterBytes = "abcdefghijkmnopqrstuvwxyz0123456789"
const (
	letterIdxBits = 6
	letterIdxMask = 1 << letterIdxBits
	letterIdxMax  = 63 / letterIdxBits
)

var src = rand.NewSource(time.Now().UnixNano())

func RandomString(n int) string {
	b := make([]byte, n)
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}
	return string(b)
}
func NewAPIKey() string {
	return RandomString(8)
}
